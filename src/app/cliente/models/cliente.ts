export interface Cliente {
    id: number;
    nome: string;
    email: string;
    endereco: string;
    casado: boolean;
    cadastro: Date;
    modificacao: Date;
}
