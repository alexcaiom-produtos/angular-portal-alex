import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Cliente } from '../models/cliente';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  url = 'http://51.79.73.86:8081/api-usuario/usuario';

  constructor(private rest: HttpClient) { }

  httpOptions = {
    headers : new HttpHeaders({ 'Content-Type' : 'application/json'})
  }

  get() : Observable<Cliente[]> {
    return this.rest.get<Cliente[]>(this.url)
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  save(c: Cliente) : Observable<Cliente> {
    return this.rest.post<Cliente>(this.url, JSON.stringify(c), this.httpOptions)
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  update(c: Cliente): Observable<Cliente> {
    return this.rest.put<Cliente>(this.url + '/' + c.id, JSON.stringify(c), this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        )
  }

  delete(c: Cliente) {
    return this.rest.delete<Cliente>(this.url + '/'  + c.id, this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        )
  } 

  handleError(error : HttpErrorResponse) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
      
    } else {
      errorMessage = 'Código do Erro: ${error.status}, ' + 'mensagem: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
