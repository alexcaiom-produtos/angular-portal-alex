import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Cliente } from 'src/app/cliente/models/cliente';

@Component({
  selector: 'app-cliente-form',
  templateUrl: './cliente-form.component.html',
  styleUrls: ['./cliente-form.component.css']
})
export class ClienteFormComponent implements OnInit {

  form : FormGroup;
  insercao : boolean = true;
  cliente : Cliente;
  titulo: string;

  constructor(
    private formBuilder: FormBuilder,
    public modal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nome : ['', Validators.required],
      endereco : ['', Validators.required],
      casado : false
    });
    if (!this.insercao) {
      this.load(this.cliente);
      if (this.cliente.nome) {
        this.titulo = this.cliente.nome; 
      } else {
        this.titulo = 'Cadastro de Clientes';
      }
    }
  }

  load(cliente) {
    this.form.patchValue(cliente);
  }

  save() {
    if (this.form.invalid) {
      return;
    }
  }

}
