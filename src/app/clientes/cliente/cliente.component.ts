import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClienteFormComponent } from '../cliente-form/cliente-form.component';
import { Cliente } from 'src/app/cliente/models/cliente';
import { ClienteService } from 'src/app/cliente/services/cliente.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  constructor(
    private modal: NgbModal,
    private service: ClienteService
  ) { }

  clientes: Cliente[] = [];
  insercao: boolean = false;
  cliente: Cliente;

  ngOnInit(): void {
    this.show();
  }

  show() {
    this.service.get().subscribe((clientes : Cliente[]) => {
      this.clientes = clientes;
    });
  }

  add() {
    const modal = this.modal.open(ClienteFormComponent);
    modal.result.then(
      this.handleModal.bind(this),
      this.handleModal.bind(this)
      );
  }

  deletar(id: string, index: number) {
    this.service.delete(this.qual(id));
  }

  editar(c: Cliente) {
    const modal = this.modal.open(ClienteFormComponent);
    modal.result.then(
      this.handleModal.bind(this),
      this.handleModal.bind(this)
    );
    modal.componentInstance.insercao = false;
    modal.componentInstance.cliente = c;
  }


  qual(id: string) : Cliente {
    if (this.clientes) {
      this.clientes.forEach(c => {
        if (c.id === new Number(id)) {
          return c;
        }
      });
    }
    return null;
  }

  handleModal(response) {
    // alert('Janela fechada');
  }

}
